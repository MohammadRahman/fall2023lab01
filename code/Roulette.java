import java.util.Scanner;
public class Roulette {
    public static void main (String[] args){
        Scanner reader = new Scanner(System.in);
         RouletteWheel wheel = new RouletteWheel();
         int userMoney = 1000;
         int moneyBet = 0;
         int numberBet = 0;
         boolean gameOver = false;

         while (!gameOver){
         System.out.println("You have " + userMoney + "$, would you like to make a bet? (yes or no)");
         String answer = reader.next();

         if (answer.equals("no")) {
            System.out.println("Game closed, you left with " + userMoney + "$");
            gameOver = true;
         }

         else if (answer.equals("yes")) {

            boolean goodBet = false;
            while (!goodBet){
            System.out.println("Enter the amount of $ you want to bet:");
            moneyBet = reader.nextInt();

            if (moneyBet > userMoney){
                System.out.println("You cannot bet more than " + userMoney + "$");
            }
            
            else{
                boolean goodNumber = false;
                while (!goodNumber){
                System.out.println("Enter the number you want to bet on:");
                numberBet = reader.nextInt();
                if (numberBet <= 37){
                System.out.println("Ok, spinning the wheel with " + moneyBet + "$");
                goodNumber = true;
                goodBet = true;
                }
                else{
                System.out.println("Error: Number doesn't exist");
                }
            }
        }
        }
        
        wheel.spin();
        System.out.println("You got: " + wheel.getValue());

        userMoney = determineResult(wheel, numberBet, moneyBet, userMoney);

        if (userMoney <= 0){
            System.out.println("You now have insufficient balance, thank you for playing!");
            gameOver = true;

        }
        
    }      
         else {
            System.out.println("Invalid response, game closed with " + userMoney + "$ balance");
            gameOver = true;
         }
        }
    }

    public static int determineResult(RouletteWheel wheel, int numberBet, int moneyBet, int userMoney){

        if (wheel.getValue() == numberBet){
            int moneyWon = moneyBet * 35;
            System.out.println("Congrats! You won " + moneyWon + "$!");
            userMoney = userMoney + moneyWon;
            System.out.println("Your current balance: " + userMoney + "$");

        }

        else {
        System.out.println("Incorrect! You lost " + moneyBet + "$!");
        userMoney = userMoney - moneyBet;
            System.out.println("Your current balance: " + userMoney + "$");

        }

        return userMoney;
    }
}
