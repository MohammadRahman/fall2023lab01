import java.util.Random;
public class RouletteWheel {
    private Random random;
    private int lastSpinNumber;

    public RouletteWheel(){
        this.random = new Random();
        this.lastSpinNumber = 0;
    }

    public void spin(){
       this.lastSpinNumber = random.nextInt(37); 
    }

    public int getValue(){
        int value = lastSpinNumber;
        return value;
    }
    }

